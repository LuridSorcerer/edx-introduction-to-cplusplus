/* ternary operator
 * This is a more compact form of an if statment.
 */

#include <iostream>
using namespace std;

int main() {

    int i = 4;
    int j = 9;

    /* the syntax is a statment to be evaluated, followed by
     * a question mark. After the question mark is code to run
     * if the statment is true, followed by a colon, and finally
     * the code to run if the statement was false.
     */

    i == 3 ? i += 10 : i--;
    cout << "i is equal to " << i << endl;

    /* this can be embedded into other statements. */
    cout << (i > j ? i : j) << " is larger" << endl;

}