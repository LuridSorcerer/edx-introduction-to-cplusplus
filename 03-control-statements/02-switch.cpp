/* switch
 * Chaining a large number of if statements can get messy and
 * difficult to follow. In these cases, a switch statement
 * can be used instead.
 */

#include <iostream>
using namespace std;

int main() {

    auto x = 3;

    /* the switch syntax is
    switch (variable_to_check)
    {
        case value1:
            code for value1
            break;   // if excluded, execution will continue
        case value2: // through following staments until break
            code2    // is encountered
            break;
        default:    // default is optional, and marks code
            code3   // to run if no other cases match,
            break;  // similar to else
    } */

    switch (x) 
    {
        case 0: 
            cout << "x is 0" << endl;
            break;
        case 1: 
            cout << "x is 1" << endl;
            break;
        case 2: 
            cout << "x is 2" << endl;
            break;
        case 3: 
            cout << "x is 3" << endl;
            break;
        default:
            cout << "x is not 0 to 3" << endl;
            break;

    }

    return 0;

}