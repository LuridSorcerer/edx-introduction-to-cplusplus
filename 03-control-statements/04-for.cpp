/* for loops
 * For loops repeat a block of code as long as a certain
 * condition is true. The starting condition and iteration
 * statement are included.
 */

#include <iostream>
using namespace std;

int main() {

    /* syntax is the keyword for, then in parentheses, 
     * the starting condition, a semicolon, the conditon
     * under which to stop, another semicolon, and finally
     * a statement to run after each iteration.
     * Following this is the code that will be run during
     * each iteration of the loop. It may be a single 
     * line, or multiple enclosed in curly braces.
     * Standard practice is to always use braces.
     */
    for (int i = 0; i < 5; i++) {
        cout << "i is " << i << endl;
    }

    return 0;


}