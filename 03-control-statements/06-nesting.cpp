/* nesting loops
 * Loops may be nested (one inside of another). This is most
 * common with for loops.
 */

#include <iostream>
using namespace std;

int main() {

    // The outer loop represents each row
    for (int i = 0; i < 5; i++) {

        // the inner loop outputs a character in each column
        for (int j = 0; j < 10; j++ ) {
            cout << i;
        }
        cout << endl;
    }

    return 0;

}