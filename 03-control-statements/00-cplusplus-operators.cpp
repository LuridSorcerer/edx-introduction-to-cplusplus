/* Operators
 */

#include <iostream>
using namespace std;

int main() {

    // + addition
    // - subtraction
    // * multiplication
    // / division
    // % modulo (divison,return remainder instead of quotient)
    // ++ increment
    // -- decrement

    // can be combined with = as prefix or postfix


    // comparisons
    // == is equal to (CAUTION: single = is assigment)
    // != is not equal to
    // >  is greater than
    // <  is less than
    // >= is greater than or equal to
    // <= is less than or equal to

    // && logical AND
    // || logical OR
    // !  logical NOT

}