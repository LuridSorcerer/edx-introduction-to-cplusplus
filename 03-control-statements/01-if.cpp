/* if
 * if statements allow code to be executed or skipped based
 * on the state of the program's data.
 */

#include <iostream>
using namespace std;

int main() {

    auto x = 5;

    /* syntax is 'if (condition) { code }'
     * The condition is evaluated. If true, the code is run.
     * Code can be a single line, or several lines enclosed
     * in curly braces. Standard practice is to always use
     * curly braces in order to make the code more clear
     * and prevent errors if additional code is added.
     */
    if (x == 5) {
        cout << "x is equal to 5" << endl;
    }

    /* else
     * else is a keyword that defined what other code to run
     * if the condition evaluated to false.
     */
    if (x == 4) {
        cout << "this shouldn't be printed" << endl;
    } else {
        cout << "x is not equal to 4" << endl;
    }

    /* else if
     * Conditions can be chained together using else if.
     * These will be checked until one condition evaluates
     * to true, then the rest will be skipped.
     * This is faster than using several if statements without
     * the else, because in that case every condition would be
     * checked, regardless of whether a previous one evaluated
     * to true.
     */
    if (x == 3) {
        cout << "x is equal to 3" << endl;
    } else if (x == 438747) {
        cout << "x is 438747, not sure how that happened" << endl;
    } else {
        cout << "x is not 3 or 43747, must be something else" << endl;
    }

    return 0;
}