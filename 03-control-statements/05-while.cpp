/* while loops
 * Similar to for loops, but while loops do not include a
 * starting conditon or iterator as part of their syntax.
 * The coder is expected to provide those things elsewhere.
 */

#include <iostream>
using namespace std;

int main() {

    char response = 'A';

    /* Syntax is simple. while keyword, followed by a 
     * condition in parentheses. As long as this condition
     * is true, the code after this definition will be
     * executed over and over. This may be a single statement
     * or a block of statements enclosed in curly braces.
     * Standard proceedure is to always use curly braces.
     */
    while (response != 'q') {
        cout << "Enter 'q' to quit: ";
        cin >> response;
    }
    cout << "first loop exited" << endl;

    /* do...while
     * Functionally the same as while loops, but will always
     * execute at least once, since the condition is checked
     * after each iteration rather than before. The keyword do
     * is first, followed by a code block to execute. Finally,
     * the while keyword and condition in parentheses. A 
     * semicolon is required after the condition, in contrast
     * to the while loop.
     */
    do {
        cout << "Enter 'e' to exit: ";
        cin >> response;
    } while (response != 'e');
    

    return 0;
}