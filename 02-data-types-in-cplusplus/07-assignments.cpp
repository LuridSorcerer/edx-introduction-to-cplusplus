#include <iostream>
using namespace std;

int main() {

    /* 
    In this first response, please paste your code that implements an array. Your array should follow these guidelines:

    Store int data types
    Store 10 values
    Have an appropriate name
    Be initialized in the declaration statement
    */
    int tenValues[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    cout << tenValues[4] << endl;


    /*
    In this second response, please paste your code that implements an enumeration. Your enumeration should follow these guidelines:

    Store the months of the year
    Start the first element as 1 rather than 0
    Use an appropriate name
    */
   enum Month {
       January = 1,
       February,
       March,
       April,
       May,
       June,
       July,
       August,
       September,
       October,
       November,
       December
   };

   Month myMonth = December;
   cout << myMonth << endl;

   /* end */
   return 0;

}


