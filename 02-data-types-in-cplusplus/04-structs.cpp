/* Structures
 * Arrays store multiple pieces of data, but they are required
 * to be of the same type. Structures are user-defined types 
 * comprised of related data, and may be composed of disparate
 * types. Structures can be used to model real-world objects or
 * data that must otherwise be processed together. 
 */

#include <iostream>
#include <string>
using namespace std;

struct coffeeBean 
{
    string name;        // name of variety
    string country;     // country of origin
    int strength;       // 1 - 10
};

int main() {

    // elements can be defined, in order, using curly braces
    coffeeBean myBean = { "Strata", "Columbia", 10 };

    // individual elements can also be accessed using dot (.) notation.
    coffeeBean newBean;
    newBean.name = "Flora";
    newBean.country = "Mexico";
    newBean.strength = 9;

    cout << "Coffee bean " << newBean.name << " is from " << newBean.country << endl;
    return 0;
}

