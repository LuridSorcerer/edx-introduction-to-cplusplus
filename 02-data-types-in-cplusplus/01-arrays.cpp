/* Every member of an array is the same type.
 * Arrays can be one-dimensional (list), two-dimensional (table), three-dimensional (cube)
 *      or even more.
 * 
 */

#include <iostream>
using namespace std;

int main() {

    // creates an array ten elements long, one dimension
    int firstArray[10]; 

    // declares an array, defines its size and elements
    int secondArray[] = { 1, 2, 3, 4, 5 }; 

    // array of size 10, defines first three elements
    int thirdArray[10] = { 1, 2, 3 }; 

    // accessing members
    // prints 3 (arrays start at 0)
    cout << "Second array, third member: " << secondArray[2] << endl;

    // ** C++ DOES NOT CHECK ARRAY BOUNDS FOR YOU ** //
    // Will print whatever is at that memory location. Or crash, maybe.
    cout << "Second array, 105th member: " << secondArray[105] << endl;

    // iterating over elements
    for (int i = 0; i < 3; i++) {
        cout << "Thrid array, Element " << i << ": " << thirdArray[i] << endl;
    }

}