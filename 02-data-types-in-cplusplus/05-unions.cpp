/* Unions
 * A Union can hold multiple different types of data, similar to a 
 * struct, but can only hold one value at a time.
 */

#include <iostream>
using namespace std;

union numericUnion {
    int intValue;
    long longValue;
    double doubleValue;
};

int main() {

    // create a union for example
    numericUnion myUnion;

    // setting intValue to 3
    myUnion.intValue = 3;
    cout << myUnion.intValue << endl;

    // setting doubleValue to 4.5, this will automatically
    // remove the intValue (sets it to zero)
    myUnion.doubleValue = 4.5;
    cout << myUnion.doubleValue << endl;
    cout << myUnion.intValue << endl;

    return 0;

}