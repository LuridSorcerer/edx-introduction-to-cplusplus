/* Strings are arrays of characters. As in C, they must be terminated with a null (\0) element
 * in order for the compiler to determine where they end.
 */

#include <iostream>
#include <string>
using namespace std;

int main() {

    char validString[6] = {'H','E','L','L','O','\0'};
    char invalidString[5] = {'H','E','L','L','O'};

    cout << validString << endl;
    cout << invalidString << endl;

    // initializing with a string literal automatically adds the null character
    // NOTE: This does not compile on Windows. On Linux it does without warnings,
    //      On Windows, it complains about the redeclaration of these variables. 
    //      Even if this is fixed, it throws an error (invalid conversion from 
    //      const char* to char [-f permissive]).
    //char validString[6] = "Hello";
    //char inferredSize[] = "Automatically right size";

    // C++ adds a string class, which is easier to use and has more capabilities than C strings.
    // It is in the std namespace, so 'using namespace std;' is required, or use std::string
    // each time.
    string myString = "This is a string";


    return 0;

}