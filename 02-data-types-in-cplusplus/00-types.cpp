#include <iostream>
using namespace std;

int main() {
    /* 
     * auto dbl = 1/3.0; // compiler automatically determines the vairable is a float
     * auto dbl = 1/3; // compiler decides dbl is an int (and truncates to 0)
     * 
     * Even if explicitly defined as float or double, division of 1/3 will result in 
     * integer division and be truncated. At least one must be explicitly defined as
     * a floating point type in order to get floating point math.
     * 
     * ex: 1/3.0, or 1.0/3
     */
    auto dbl = 1/3.0;

    cout << "Value of 1/3: " << dbl << endl;
    return 0;
}