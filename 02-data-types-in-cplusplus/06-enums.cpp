/* Enumerations
 * Enums allow you to create your own type with a limited set
 * of possible values, which are pre-defined. The compiler 
 * can automaticall determine what the underlying values are,
 * or they can be manually specified.
 */

#include <iostream>
using namespace std;

// if "= 1" is omitted, the internal values will start at 0 and increase by 1 for
// each successive element. Otherwise, values start from the specified value.
enum Day {Sunday /* = 1 */, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday };

int main() {

    Day payday = Friday;
    cout << payday; // outputs 5, not "Friday"
    
    return 0;

}