/* Functions
 * A function is a discreet block of program code that can be
 * called to perform a specific task. It is essential a group
 * of instructions that has been given a name and can have
 * data passed to it for processing and a result passed back.
 */

#include <iostream>
using namespace std;

/* This function is defined above its use. The syntax is
 * the return type, followed by the name, then the arguments
 * in parentheses, followed by the function instructions 
 * in curly braces.
 */
int Sum(int x, int y) {
    return x + y;
}

int main() {

    int a = 4, b = 8;

    cout << "The result is " 
         << Sum(a,b) << endl;
    
    return 0;

}