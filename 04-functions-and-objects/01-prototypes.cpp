/* Functions may be declared and implemented at once, but
 * it is more common to see the function prototype (definition)
 * separate from its code (implementation). Function prototypes
 * can be at the top of the file they are implemented in,
 * or can be in a header file (.h) that is #included in a 
 * file where the function is used.
 */

// library files are included with angle brackets
#include <iostream>

// local headers are included with a relative path and quotes
#include "01-prototypes.h"

int main() {
    std::cout << "The sum is " << Sum(3,5) << std::endl;
    return 0;
}