#include <iostream>
using namespace std;

class Rectangle {
    public:

        // constructors
        // These will initialize the local variable for us.
        // It is overloaded so that we can either provide
        // initial values as before, or leave it blank for
        // the time being.
        Rectangle() { _width = _height = 0; }
        Rectangle(int initial_width, int initial_height) {
            _width = initial_width;
            _height = initial_height;
        }

        void resize(int new_width, int new_height) {
            _width = new_width;
            _height = new_height;
        }

        // since these members do not change the member 
        // variables, we can use the const keyword, thus 
        // allowing them to be called on a const object of
        // this class.
        int get_area() const { return this->_width * this->_height; }
        int get_width() const { return _width; }
        int get_height() const { return _height; }

    private:
        // Member variables can no longer be accessed directly.
        // Instead, they can only be read or written using the
        // member functions we define. This prevents misuse of
        // the variables, and lets us do error checking and other
        // operations on incoming data.
        int _width;
        int _height;
};

int main() {
    
    Rectangle uninitialized{};
    uninitialized.resize(89,55);

    cout << "Height: " << uninitialized.get_height() << endl;

    // objects can also be declared const
    Rectangle const imutable_rectangle(44,5);

    // only member functions that have been delcared const
    // may be called.
    cout << "Const rectangle area: " << imutable_rectangle.get_area() << endl;

    return 0;
}