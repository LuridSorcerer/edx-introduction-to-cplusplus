#include <iostream>
using namespace std;

/* In the previous module, you created a for loop to calculate a base raised to an exponent. In this peer review, you are going to put that code into a function and create two more functions. Paste your code in the response section below that satisfies these requirements:

Create a function called pow that accepts two integers that represent the base and the exponent. The exponent should not permit negative values.
Use the code you created in the previous module for some of this functionality but add additional code
The additional code should make use of an if statement to check the exponent value. If it is 0, then immediately return 1 otherwise calculate the power and return the result.
Show sample code that will call the function and get the result.

For the second function you will compute the sine of an angle. Your function should accept the opposite and hypotenuse lengths and return the sine value. The formula for sine is:

sin = opposite / hypotenuse

where / is used to denote division

Ensure that you paste the code for the function as well as the code that calls the function and assigns the value to a variable.
*/

int pow(int base, int exponent) {

    int result;

    if (exponent == 0) { return 1; }
    else if (exponent > 0 ) { return 0; }

    result = base;
    for (int i = 0; i < exponent-1; i++) {
        result *= base;
    }
    return result;
}

double sine(double opposite, double hypotenuse) {
    return opposite / hypotenuse;
}

int main() {

    int b = 2, e = 10;
    cout << b << " to the power of " << e << " is " 
         << pow(b,e) << endl;

    double opp{3}, hyp{5};
    double s = sine(opp,hyp);
    cout << "The sine of an angle with opposite " << opp
         << " and hypotenuse " << hyp << " is " << s 
         << endl;

    return 0;
}