/* classes
 * A class lets you define your own types. Like a struct, 
 * a class can have member variables. These may be public
 * or private. It may also have functions that operate on
 * these member variables, and the functions may be either
 * public or private as well.
 */

#include <iostream>
using namespace std;

class Rectangle {
    public:
        int _width;
        int _height;
};

int main() {

    Rectangle uninitialized;      // values are not defined
    Rectangle defaultValue{};     // initializes to default value for type
    Rectangle definedValues{1,2}; // values are assigned as given

    return 0;

}