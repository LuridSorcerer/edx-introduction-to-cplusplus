/*
Expanding Your Knowledge
For the second part of this assignment, you will create a function to perform the average of values in an array. To complete this portion, your code should match the following requirements:

Create a function called avg that will accept an integer array. You may not have seen this in a demo but passing an array to a function is no different than passing any other data type. Search the Internet for an example if you can't figure out how to pass the array.
Create an array of integers, at least 5 in size, and pass it to the avg function.
Inside the function, use a loop to iterate over the array elements and calculate the average.
Return the average back to the code that called it.
*/

#include <iostream>
using namespace std;

double avg(int a[], int count) {
    double total{0};
    for (int i = 0; i < count; i++) {
        total += a[i];
    }
    return total / count;
}

int main() {

    int myInts[] = { 3, 4, 5, 6, 70 };

    double average = avg(myInts,5);
    cout << "The average is " << average << endl;

    return 0;

}